import os

import psycopg
from dotenv import load_dotenv
from psycopg import OperationalError

load_dotenv()

# Connection settings
database = os.environ.get("POSTGRES_DB")
user = os.environ.get("POSTGRES_USER")
password = os.environ.get("POSTGRES_PASSWORD")
host = os.environ.get("POSTGRES_HOST")
port = os.environ.get("POSTGRES_PORT_HOST")


def create_connection(db_name, db_user, db_password, db_host, db_port):
    connection = None
    try:
        connection = psycopg.connect(
            dbname=db_name,
            user=db_user,
            password=db_password,
            host=db_host,
            port=db_port,
        )
        print("Connection to PostgreSQL DB successful")
    except OperationalError as e:
        print(f"The error '{e}' occurred")
    return connection


def execute_query(connection, query):
    connection.autocommit = True
    cursor = connection.cursor()
    try:
        cursor.execute(query)
        print("Query executed successfully")
    except OperationalError as e:
        print(f"The error '{e}' occurred")


def execute_read_query(connection, query):
    cursor = connection.cursor()
    result = None
    try:
        cursor.execute(query)
        result = cursor.fetchall()
        return result
    except OperationalError as e:
        print(f"The error '{e}' occurred")


connection = create_connection(
    db_name=database, db_user=user, db_password=password, db_host=host, db_port=port
)

if __name__ == "__main__":
    # Queries to setup database structure
    create_users_table = """
    -- Users Table
    CREATE TABLE IF NOT EXISTS users (
        id SERIAL PRIMARY KEY,
        name VARCHAR(255),
        age INTEGER,
        gender VARCHAR(50),
        nationality VARCHAR(255)
    );
    """

    create_posts_table = """
    -- Posts Table
    CREATE TABLE IF NOT EXISTS posts (
        id SERIAL PRIMARY KEY,
        user_id INTEGER REFERENCES users(id),
        title VARCHAR(255),
        description TEXT
    );
    """

    create_comments_table = """
    -- Comments Table
    CREATE TABLE IF NOT EXISTS comments (
        id SERIAL PRIMARY KEY,
        user_id INTEGER REFERENCES users(id),
        post_id INTEGER REFERENCES posts(id),
        text TEXT
    );
    """

    create_likes_table = """
    -- Likes Table
    CREATE TABLE IF NOT EXISTS likes (
        id SERIAL PRIMARY KEY,
        user_id INTEGER REFERENCES users(id),
        post_id INTEGER REFERENCES posts(id)
    );
    """

    create_email_table = """
    -- Email Table
    CREATE TABLE IF NOT EXISTS emails (
        id SERIAL PRIMARY KEY,
        user_id INTEGER REFERENCES users(id),
        email VARCHAR(255) UNIQUE
    );
    """

    # executing queries
    execute_query(connection=connection, query=create_users_table)
    execute_query(connection=connection, query=create_posts_table)
    execute_query(connection=connection, query=create_comments_table)
    execute_query(connection=connection, query=create_likes_table)
    execute_query(connection=connection, query=create_email_table)

    # Checking database structure
    get_table_names = """
    -- Get Table Names
    SELECT table_name
    FROM information_schema.tables
    WHERE table_schema = 'public'
    """
    table_names = execute_read_query(connection=connection, query=get_table_names)

    print(f"\nTables in {database}:")
    print("-" * 30)
    print(*[table[0] for table in table_names], sep="\n")
    print("-" * 30)
